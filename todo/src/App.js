import React, { useState, useEffect } from 'react';
import './App.css';
//Importing Components
import Form from './components/Form'
import TodoList from './components/TodoList'


function App() {
  
  // Use states -> variables that need to be checked when the user interacts with the site
  // Input text and description used for when user inputs information into the forms
  const [inputText, setInputText] = useState("");
  const [description, setDescription] = useState("");

  // Todos used for managing the list of todos that the user creates, filter for filtering todos based on status
  const [todos, setTodos] = useState([]);
  const [filteredTodos, setFilteredTodos] = useState([])

  // Status used to check whether a todo is complete or incomplete for the filter.
  const [status, setStatus] = useState('all');


  // Run once on app startup, this useEffect checks if there are any localTodos saved, if so, load them
  useEffect(() => {
    getLocalTodos();
  }, []);

  // Run everytime on status change and todos change, status change -> change the filter to what the user is asking for
  // todos change -> If a todo is added / taken away, save the new list
  useEffect(() => {
    filterHandler();
    saveLocalTodos();
  }, [todos, status])  

  // Only show the todos with the correct completed value [whether that is true, false, or all of them]
  const filterHandler = () => {
    switch(status) {
      case 'completed':
        setFilteredTodos(todos.filter(todo => todo.completed === true));
        break;
      case 'uncompleted':
        setFilteredTodos(todos.filter(todo => todo.completed === false));
        break;
      default:
        setFilteredTodos(todos)
        break;
    }
  }

  // Save todos local storage for any future loading.
  const saveLocalTodos = () => {
    localStorage.setItem('todos', JSON.stringify(todos));
  };

  // If there are no todolists on localstorage, create a new list, otherwise load them
  const getLocalTodos = () => {
    if(localStorage.getItem('todos') === null) {
      localStorage.setItem('todos', JSON.stringify([]));
    }
    else {
      let todoLocal = JSON.parse(localStorage.getItem('todos'))
      setTodos(todoLocal)
    }
  };

  return (
    <div className="App">
      <header>
        <h1>Todo List</h1>
      </header>
      <Form 
      inputText={inputText} 
      description={description}
      todos={todos} 
      setTodos={setTodos} 
      setInputText={setInputText}
      setStatus={setStatus}
      setDescription={setDescription}
      />
      <TodoList 
      setTodos={setTodos} 
      todos={todos}
      filteredTodos={filteredTodos}
      />
    </div>
  );
}

export default App;