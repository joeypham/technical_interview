import React from 'react';


const Todo = ({text, todo, todos, setTodos, description}) => {
    
    // Handler for the delete button, deletes specific todo on button press based on the id of the todo
    // Essentially scans the list for the todo with the same id, and it filters it out / deletes it
    const deleteHandler = () => {
        setTodos(todos.filter((el) => el.id !== todo.id));
    };
    
    // Handler for the complete button, if the complete button is pressed, flip the completed value [because default is false]
    const completeHandler = () => {
        setTodos(todos.map((item) => {
            if(item.id === todo.id) {
                if(item.completeDate === "") {
                    return {
                        ...item, completed: !item.completed, completeDate: Date().toLocaleString().split('G')[0]
                    }
                } else {
                    return {
                        ...item, completed: !item.completed, completeDate: ""
                    }
                }
            }
            return item
        }))
    };
    
    // Handler for expand button, if pressed, expands the todo to show the time listed and description of task
    const todoHandler = () => {
        setTodos(todos.map((item) => {
            if(item.id === todo.id) {
                return {
                    ...item, expanded: !item.expanded
                }
            }
            return item
        }));
    }

    // Handler for edit description, allows user to edit description of task
    const descriptionHandler = () => {
        setTodos(todos.map((item) => {
            if(item.id === todo.id) {
                const newDescription = window.prompt("Type your new description here!")
                return {
                    ...item, description: newDescription
                }
            }
            return item
        }))
    }

    return (
        <div type="button" className = "todo">
            <li className={`todo-item ${todo.completed ? "completed" : ""}`}>{text}
                <div className={`${todo.expanded ? "expanded" : "collapsed"}`}>Created: {todo.date}</div>
                <div className={`${todo.expanded ? "expanded" : "collapsed"}`}>Description: {todo.description}
                    <button onClick={descriptionHandler} className = "description-btn">
                        <i className = "fas fa-edit"></i>
                    </button>
                </div>
                <div className={`${todo.expanded ? "expanded" : "collapsed"}`}>Completed: {todo.completeDate}</div>
            </li>

            <button onClick={todoHandler} className ="expand-btn">
                <i className = 'fas fa-expand'></i>
            </button>

            <button onClick={completeHandler} className = "complete-btn">
                <i className ="fas fa-check"></i>
            </button>
            <button onClick={deleteHandler} className = "trash-btn">
                <i className ="fas fa-trash"></i>
            </button>
        </div>
    );
}

export default Todo;