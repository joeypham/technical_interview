import React from 'react'

const Form = ({inputText, setInputText, todos, setTodos, setStatus, description, setDescription}) => {

    // Change inputText whenever something is put into the search bar
    const inputTextHandler = (e) => {
      setInputText(e.target.value);
    };

    // Same as inputTextHandler, but used for description option
    const descriptionTextHandler = (e) => {
      setDescription(e.target.value);
    };

    // Handles the submit button, assigns default values to the todo while setting inputText to whatever use input
    const submitTodoHandler = (e) => {
      e.preventDefault();
      setTodos([
        ...todos, {text: inputText, completed: false, id: Math.random() * 1000, date: Date().toLocaleString().split('G')[0], expanded: false, description:description, completeDate:""},
      ]);
      setInputText("");
      setDescription("");
    };

    // Set the status of a todo to completed/incomplete based on value, default value is incomplete
    const statusHandler = (e) => {
      setStatus(e.target.value)
    }

    return(
    <form>
      <input value = {inputText} onChange ={inputTextHandler} type="text" className="todo-input" placeholder="Todo" />
      <input value = {description} onChange = {descriptionTextHandler} type="text" className="description-form" placeholder="Description" />
      <button onClick = {submitTodoHandler} className="todo-button" type="submit">
        <i className="fas fa-plus-square"></i>
      </button>
      <div className="select">
        <select onChange={statusHandler} name="todos" className="filter-todo">
          <option value="all">All</option>
          <option value="completed">Completed</option>
          <option value="uncompleted">Uncompleted</option>
        </select>
      </div>
    </form>
    )
}

export default Form;